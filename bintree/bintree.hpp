
#pragma once
#include <iostream>
//#include "../list/linked_list.hpp"
#include <vector>
#include <string>

using namespace std;

template <class T>
class Bintree;


template <class T>
struct BTNode{

    friend class Bintree<T>;

    BTNode(const typename Bintree<T>::value_type value){
        sx_child = nullptr;
        dx_child = nullptr;
        parent = nullptr;
        number_child =0;
        this->value = value;
    }

    BTNode* parent;
    BTNode* sx_child;
    BTNode* dx_child;
    typename Bintree<T>::value_type value;
    int number_child;
};


template <class T>
class Bintree{
public:
    typedef T value_type;
    typedef BTNode<T> Node;

    Bintree();
    ~Bintree();
    Bintree(const Bintree<T>&);

    Node* insertdx(Node* , const value_type);
    void create();
    Node* insertsx(Node* , const value_type);
    Node* sx(Node*) const;
    Node* dx(Node*) const;
    Node* parent(Node*) const;
    bool isEmpty_sx(Node*) const;
    bool isEmpty_dx(Node*) const;
    bool isEmpty() const;
    value_type read(Node*) const;
    void write(Node*, value_type); 
    Node* insert_root(const value_type);
    Node* get_root() const;
    void print(std::ostream&, Node*, int, std::string, std::string space);
    void remove(Node*);
    size_t size() const;
    bool leaf(Node*) const;

    int depth() ;
    int _depth(Node*);
    

    void preOrder();
    void postOrder();
    void inOrder();
    void delete_pair_leaf();
    void delete_pair(Node* n);
    void reset_number_child();
    void reset_child(Node *n);
    int get_number_child(Node* n);
    int number_child(Node* n);
    void mutation(Bintree<T>& tree, Node* n , Node* m);
    void traversalPreOrder(Node*);
    void traversalPostOrder(Node*);
    void traversalInOrder(Node*);
    void parse(const std::string expression);
    int livell(Node*);
    int _livell(Node*, Node*, int);

    //void average_level();
    //void _average_level(Node*,std::vector<Linked_list<int>>&);

    template <class Z>
    friend std::ostream& operator<<(std::ostream&, Bintree<Z>&);
    void operator=(const Bintree<T>&);

private:
    Node* root;
    size_t dsize;

    void _erase(Node*);
    Node* _copy(Node*, Node*);
 
};


template<class T>
void Bintree<T>::preOrder(){
    traversalPreOrder(root);
}

template <class T>
void Bintree<T>::traversalPreOrder(Node* node){
    if(node != nullptr){
        std::cout << node->value;
        traversalPreOrder(node->sx_child);
        traversalPreOrder(node->dx_child);
    }
}

template<class T>
void Bintree<T>::inOrder(){
    traversalInOrder(root);
}



template <class T>
void Bintree<T>::traversalInOrder(Node* node){
    if(node != nullptr){
        traversalInOrder(node->sx_child);
        std::cout << node->value;
        traversalInOrder(node->dx_child);
    }
}

template<class T>
void Bintree<T>::postOrder(){
    traversalPostOrder(root);
}

template <class T>
void Bintree<T>::traversalPostOrder(Node* node){
    if(node != nullptr){
        traversalPostOrder(node->sx_child);
        traversalPostOrder(node->dx_child);
        std::cout << node->value;
    }
}

template<class T>
size_t Bintree<T>::size() const{
    return this->dsize;
}

template <class T>
Bintree<T>::Bintree(const Bintree<T>& tree){
    create();
    root = _copy(nullptr, tree.get_root());

}

template <class T>
typename Bintree<T>::Node* Bintree<T>::_copy(Node* parent_node, Node* node){

    if(node != nullptr){
        Node* new_node = new Node(node->value);
        new_node->parent = parent_node;
        new_node->sx_child = _copy(new_node, node->sx_child);
        new_node->dx_child = _copy(new_node, node->dx_child);

        dsize++;

        return new_node;

    }else{
        return nullptr;
    }
}

template <class T>
void Bintree<T>::create(){
    root = nullptr;
    dsize = 0;
}

template <class T>
void Bintree<T>::operator=(const Bintree<T>& tree){
    _erase(root);
    create();
    root = _copy(nullptr, tree.get_root());
}


template <class T>
void Bintree<T>::write(Node* n, value_type value){
    if(n != nullptr){
        n->value = value;
    }else{
        throw "Nodo nullo";
    }
}

template <class T>
Bintree<T>::~Bintree(){
    _erase(root);
    
}

template <class T>
void Bintree<T>::_erase(Node* n){

    if(n != nullptr){
        _erase(n->sx_child);
        _erase(n->dx_child);
        
        delete n;

        dsize--;
    }
}

template <class T>
void Bintree<T>::remove(Node* n){

    bool isRoot = false;
    if(n->parent != nullptr){
        if(n->parent->sx_child == n){
            n->parent->sx_child = nullptr;
        }else{
            n->parent->dx_child = nullptr;
        }
    }else{
        if(n == root){
            isRoot = true;
        }
    }

    _erase(n);

    if(isRoot){
        create();
    }
    
}

template <class T>
typename Bintree<T>::value_type Bintree<T>::read(Node* n) const{
    return n->value;
}

template <class T>
bool Bintree<T>::isEmpty() const{
    return (root == nullptr);
}

template <class T>
bool Bintree<T>::isEmpty_dx(Node* n) const{
    return (n->dx_child == nullptr);
}

template <class T>
bool Bintree<T>::isEmpty_sx(Node* n) const{
    return (n->sx_child == nullptr);
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::parent(Node* n) const{
    return n->parent;
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::dx(Node* n) const{
    return n->dx_child;
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::sx(Node* n) const{
    return n->sx_child;
}

template <class T>
std::ostream& operator<<(std::ostream& os, Bintree<T>& tree){
    if(tree.get_root() != nullptr){
        tree.get_number_child(tree.get_root());
        tree.print(os, tree.get_root(), 0, "","");
    }
    return os;
}


template <class T>
void Bintree<T>::print(std::ostream& os, Node* n, int count, std::string chr, std::string space){
    if(space.length() > 0){
        if(space[space.size() - 1] != ' '){
            os << space.substr(0, space.size() - 3);
        }else{
            os << space.substr(0, space.size() - 1);
        }

    }else{
        os << space;
    }
   
    os << chr;
    os << n->value;
    os <<"      c"<< n->number_child;
  
   
    if(n->dx_child != nullptr){
        if(n->sx_child != nullptr){
            chr =  "├";
        }else{
            chr =  "└";
        }
        
        os << std::endl;
        if(n->dx_child != nullptr and n->sx_child != nullptr and n->dx_child->dx_child != nullptr){
            print(os,n->dx_child,count+1,chr, space + "\u2502");
      
        }else{
            print(os,n->dx_child,count+1,chr, space + " ");
           
        }
    }

    if(n->sx_child != nullptr){
        chr = "└";
        os << std::endl;
        print(os,n->sx_child,count+1, chr, space + " ");
    }  
}


template <class T>
Bintree<T>::Bintree(){
    create();
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::insertdx(Node* n, value_type value){
    if(n->dx_child == nullptr){
        n->dx_child = new Node(value);
        n->dx_child->parent = n;
        dsize++;

        return n->dx_child;
    }else{
        throw "Nodo presente";
    }
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::insertsx(Node* n, value_type value){
    if(n->sx_child == nullptr){
        n->sx_child = new Node(value);
        n->sx_child->parent = n;
   
        dsize++;

        return n->sx_child;
    }else{
        throw "Nodo presente";
    }
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::insert_root(const value_type value){
    if(root == nullptr){
        root = new Node(value);
        dsize++;
        return root;
    }else{
        throw "Root gia presente";
    }
}

template <class T>
typename Bintree<T>::Node* Bintree<T>::get_root() const{
    return root;
}

template <class T>
bool Bintree<T>::leaf(Node* n) const{
    return (isEmpty_sx(n) and isEmpty_dx(n));
}

template <class T>
void Bintree<T>::delete_pair_leaf() {
    delete_pair(get_root());

}

template <class T>
void Bintree<T>::delete_pair(Node* n) {
    if(n != nullptr){
  
        if(leaf(n)) {
                if(read(n)%2 ==0)
                    remove(n);
        }
        else {
            delete_pair(sx(n));
            delete_pair(dx(n));
        }
    }

}



template<class T>
int Bintree<T>::get_number_child(Node* n) {
    reset_number_child();
    return number_child(n);
}

template<class T>
int Bintree<T>::number_child(Node* n) {
     if(n != nullptr){
        if(!leaf(n)) 
        {

            if(!isEmpty_sx(n)) 
               n->number_child=n->number_child + number_child(sx(n))+1; 
            if(!isEmpty_dx(n))
                n->number_child= n->number_child + number_child(dx(n))+1;
            return n->number_child;
        }
    }

    return 0;

}

template<class T>
void Bintree<T>::reset_number_child() {
    reset_child(get_root());
}

template<class T> 
void Bintree<T>::reset_child(Node * n) {
     if(n != nullptr){
        if(!leaf(n)) 
        {
            if(!isEmpty_sx(n)) 
               n->number_child=0;
            if(!isEmpty_dx(n))
                n->number_child=0;
        }
    }
}


template<class T>
void Bintree<T>::mutation(Bintree<T>& tree, Node* n , Node* m) { 
    auto m1 = tree._copy(nullptr,n);
    _erase(n);
    n = _copy(nullptr,m);
    tree._erase(m);
    m= tree._copy(nullptr,m1);
    _erase(m1);
    
 
}

template<class T> 
int Bintree<T>::depth()  {
    int depth = _depth(root);
    return depth;
}

template<class T>
int Bintree<T>::_depth(Node* n) {


    if (n == nullptr) 
        return 0; 

    if (leaf(n)) 
        return 1; 

    if (!isEmpty_dx(n)) 
        return _depth(dx(n)) + 1; 
  

    if (!isEmpty_sx(n)) 
        return _depth(sx(n)) + 1; 

    if(_depth(sx(n))> _depth(dx(n)))
        return _depth(sx(n)) + 1;
    
    else
        return _depth(dx(n)) + 1;

}
template<class T>
void Bintree<T>::parse(const std::string expression) {
    
    Node* n= insert_root('n');
    Node* m;
        for (auto &c : expression) {
            if (c== '(') {
                m= n;
            }
            else if (c==')') {
                 if(isEmpty_sx(m))
                    insertsx(m,read(m));
            else if(isEmpty_dx(m))
                    insertdx(m,read(m));
             
            }
                else if(c =='+'|| c== '-'|| c== '*'|| c==':') {
                   if(read(get_root())=='n') 
                        write(get_root(),c);
                        else if(isEmpty_dx(n))
                                insertdx(n,c); 
                            else 
                            {
                                n=dx(n);
                                
                                if(isEmpty_sx(n))
                                    insertsx(n,read(n));
                                else if(isEmpty_dx(n))
                                    insertdx(n,read(n));
                                write(n,c);  
                               
                            }    
                   
                }
                else if (c =='0'||'1'||'2'||'3'||'4'||'5'||'6'||'7'||'8'||'9') {
                    if(isEmpty_sx(n))
                        insertsx(n,c);
                    else if(isEmpty_dx(n))
                        insertdx(n,c);
                   
                }

           
        }
        
    
}

template<class T>
int Bintree<T>::livell(Node* N) {
    return _livell(root, N, 0);
}


template<class T>
int Bintree<T>::_livell(Node* n, Node* m, int livell) {
    if(n==nullptr)
        return 0;

    if(n==m) 
        return livell; 
           
           
    int l=_livell(sx(n),m,livell+1);
    if(l !=0)
        return l;
    l= _livell(dx(n),m,livell+1);
        return l;
        
      
}

/*

template<class T>
void Bintree<T>::average_level() {
    vector<Linked_list<int>> l(depth(),Linked_list<int>());
    int sum ,average=0;

    int depth =this->depth(); 
    cout<<"average for level bintree"<<endl;
    _average_level(root, l);

    for(int i=0; i< depth; i++) {

        sum=0;

        if(l[i].size() != 0 ) {

            for(auto n = l[i].begin(); !l[i].end(n); n=l[i].next(n)) 
                sum = sum + l[i].read(n);

            cout <<sum/l[i].size() <<endl;
            
            }
        }
    cout<<"+++++++++++++++++++"<<endl;
}



template<class T>
void Bintree<T>::_average_level(Node* n, std::vector<Linked_list<int>>& l) {
     if(n==nullptr)
        return ;
    l[livell(n)].push_back(read(n));
    if(!leaf(n))
    {
            _average_level(sx(n),l);
            _average_level(dx(n),l);
    }


}

*/