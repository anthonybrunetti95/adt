
#include "bintree.hpp"

template<class T>
class balanced_tree{

public :

/* Stabilisce se l’albero `e bilanciato in altezza.
* Un albero binario `e bilanciato in altezza se a) `e vuoto, o b) se per ogni nodo
* le altezze dei suoi due sottoalberi differiscono al pi`u di uno e i due sottoalberi
* sono bilanciati in altezza.
*/
balanced_tree();
bool is_height_balanced(const Bintree< T > &B);
bool controll_balanced(BTNode<T>* node,const Bintree<T> &B); 
int max(int n, int m) const ;
int height(BTNode<T>* node, const Bintree<T> &B) const;
/* Stabilisce se tutti i nodi non foglia dell’albero hanno esattamente due figli */
bool complete_nodes(const Bintree< T > &B);
bool complete_node(BTNode<T>* node, const Bintree<T> &B);


};

template<class T>
balanced_tree<T>::balanced_tree() {}

template <class T>
bool balanced_tree<T>::is_height_balanced(const Bintree<T> &B) {
	return controll_balanced(B.get_root(),B);	
}


template <class T>
bool  balanced_tree<T>::controll_balanced( BTNode<T>* node, const Bintree<T> &B){


	int hsx;
	int hdx;
	if(node ==nullptr){
		return 1;
		 
	}	 
	else
	{
		hsx = height(B.sx(node),B);
		hdx = height(B.dx(node),B);
		if( (hsx - hdx)<=1 && (hsx - hdx)>=-1 && controll_balanced(B.sx(node),B) && controll_balanced(B.dx(node),B))
			return 1;	 				
	}
	return 0;
}

template <class T>
int balanced_tree<T>::height(BTNode<T>* node,  const Bintree<T> &B)const  {
	if (node ==nullptr) 
		return 0;
	return 1+max(height(B.sx(node),B),height(B.dx(node),B));


}

template<class T>
int balanced_tree<T>::max(int n, int m) const {
	 return (n >= m) ? n : m; 
}

template<class T>
bool balanced_tree<T>::complete_nodes(const Bintree< T > &B) {
	return complete_node(B.get_root(),B);

}


template<class T>
bool balanced_tree<T>::complete_node(BTNode<T>* node, const Bintree<T> &B) {

	if(node ==nullptr )
		return 0; 
	else if (B.leaf(node))
		return 1;	 
	else 
		return (complete_node(B.sx(node),B) && complete_node(B.dx(node),B));
	return 0;			
}


