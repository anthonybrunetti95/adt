#include "bintree.hpp"


template<class T>
class zero_one_binary_tree{
public:
/* Stabilisce se l’albero rispetta le propiet`a di un albero zero-one, ovvero:
* 1) la radice ha valore 0;
* 2) ogni nodo 0, ha come figli due nodi 1;
* 3) ogni nodo 1, ha come figli due nodi 0.
*/
bool is_zero_one(const Bintree<T> &B);
bool control(BTNode<T>* node, const Bintree<T> &B, bool flag);
/* Restituisce il numero di nodi 0 dell’abero */
//int zero_nodes(const Bintree<T> &B);
};



template<class T >
bool zero_one_binary_tree<T>::is_zero_one(const Bintree<T> &B) {
	return control(B.get_root(),B,0);
}

template<class T>
bool zero_one_binary_tree<T>::control(BTNode<T>* node, const Bintree<T> &B, bool flag) {
	if (node== nullptr) 
		return 0;

	else if(B.leaf(node)) 
		return B.read(node) ==flag;
	
	else if(control(B.sx(node),B,!flag) && control(B.dx(node),B,!flag)) 
		return B.read(node) ==flag;
	
	return 0;		
}


