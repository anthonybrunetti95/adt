#include "../list/linked_list.hpp"
#include <iostream>
#include <string>

using namespace std;



struct pair_rs
{
 friend class Recomander_systemer;

 typedef string utente;
 typedef string prodotto;
pair_rs(utente u, prodotto p, int g) {
	user =u;
	item=p;
	giudizio=g;
}
	friend std::ostream& operator<<(std::ostream& os, const pair_rs	pr){
        os << "{" << pr.user << " : " << pr.item << " : "<<pr.giudizio <<"}";
        return os;
    }

private:
	utente user;
	prodotto item;
	int giudizio =0;
};

class Recomander_systemer {
public:
	 typedef typename pair_rs::utente utente;
	 typedef typename pair_rs::prodotto prodotto;


	void crea_rs();
	void registra_utente(utente);
	void registra_prodotto(prodotto);
	void gradisce(utente,prodotto ,int);
	int gradimenti(prodotto);
	double media_gradimenti(prodotto);
	int gradimenti_simili(utente,utente);
	void print();
private:
	int _gradimenti_simili(prodotto, utente);
	Linked_list<utente> utenti;
	Linked_list<prodotto> prodotti;
	Linked_list<pair_rs*> rs_db;

};



void Recomander_systemer::crea_rs() {

	utenti.create();
}


void Recomander_systemer::registra_utente(utente u) {
		if(utenti.find(u)==nullptr){
			utenti.push_back(u);
			return;
		}
	
		utenti.push_back(u);
		std::cout<<utenti;
}


void Recomander_systemer::registra_prodotto(prodotto p) {
		if(prodotti.find(p) == nullptr){
			prodotti.push_back(p);
			return ;
		}

		prodotti.push_back(p);
}

void Recomander_systemer::gradisce(utente u, prodotto p, int g) {
	pair_rs* p1=new pair_rs(u,p,g);
	if(utenti.find(u)!=nullptr && prodotti.find(p)!=nullptr)
		if(rs_db.find(p1)==nullptr)
			rs_db.push_back(p1);
}

int Recomander_systemer::gradimenti(prodotto p) {
	
	int count=0;
	if(!rs_db.empty())
		for(auto i=rs_db.begin();!rs_db.end(i); i= rs_db.next(i)){
			auto p1= rs_db.read(i);
			if(p1->item == p)
				count++;
		}


	return count;	
}


double Recomander_systemer::media_gradimenti(prodotto p) {
	double sum =0;
	int cum = 0;
	if(!rs_db.empty())
		for(auto i=rs_db.begin();!rs_db.end(i); i= rs_db.next(i)){
			auto p1= rs_db.read(i);
			if(p1->item == p) {
				sum += p1->giudizio;
				cum++;
			}
		}
	return sum/cum; 
}


int Recomander_systemer::gradimenti_simili(utente u , utente u1) {
	int cum = 0;
	prodotto p;
	if(!rs_db.empty())
		for(auto i=rs_db.begin();!rs_db.end(i); i= rs_db.next(i)){
			auto p1= rs_db.read(i);
			if(p1->user == u ) {
				p=p1->item;
				cum+= _gradimenti_simili(p,u1);
			}
		}
	return cum;
}

int Recomander_systemer::_gradimenti_simili(prodotto p, utente u1) {
	int cum =0;
		for(auto i=rs_db.begin();!rs_db.end(i); i= rs_db.next(i)){
			auto p1= rs_db.read(i);
			if(p1->user ==u1) 
				if(p1->item == p)
					cum ++;
		}
return cum;
}

void Recomander_systemer::print() {
	std::cout<<"[";
	for(auto i=rs_db.begin();!rs_db.end(i); i= rs_db.next(i)){
			auto *n = rs_db.read(i);
			auto d=*n;
			std::cout<<d;
		}
	std::cout<<"]"<<endl;	
}