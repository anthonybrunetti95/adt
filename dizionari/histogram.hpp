//============================================================================
// Name        : anthony_brunetti_3_setembre.cpp
// Author      : Anthony
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include "dizionari.hpp"

class histogram
{
public:

	histogram();
	void add(int v);
	void remove(int v);
	int mode();
	double mean();
	void print();
	
private:

	Tabella_Hash<int,int>* dizionario;

};

histogram::histogram() {
	dizionario = new Tabella_Hash<int,int>(100);
}


void histogram::add(int v) {

	auto *n =dizionario;
	auto d=*n;
	
	Linked_list<int> keys= d.keys();
	if(!keys.empty())
		for(auto i= keys.begin();!keys.end(i);i=keys.next(i))
			if(keys.read(i)== v){
				d[v].value++;
				*n=d;
				dizionario=n;
				return ;
			}

	d.insert(v,1);
	*n=d;
	dizionario=n;
		
}

void histogram::remove(int v) {

	auto *n =dizionario;
	auto d=*n;
	if(d.containsValue(v))
		d.erase(v);
	*n=d;
	dizionario=n;

}

int histogram::mode() {
	int max=0;
	Linked_list<int> l= dizionario->values();
	
	for(auto i= l.begin();!l.end(i);i=l.next(i))
		if(max<l.read(i))
			max=l.read(i);
	return dizionario->value_to_key(max);

}


double histogram::mean() {
	
	auto *n =dizionario;
	auto d=*n;
	double sum=0;
	Linked_list<int> keys= d.keys();
	for(auto i= keys.begin();!keys.end(i);i=keys.next(i))
			sum =sum + d[keys.read(i)].value;	
	return sum/dizionario->keys().size();

}


void histogram::print() {
	

	auto *n =dizionario;
	auto d=*n;

	Linked_list<int> keys= d.keys();
	std::cout<<d<<endl;

	for(auto i= keys.begin();!keys.end(i);i=keys.next(i)) {

			std::cout<<keys.read(i);
			for(int j=0 ; j<d[keys.read(i)].value; j++)
				std::cout<<"*";
		std::cout<<""<<std::endl;
	}
	
}