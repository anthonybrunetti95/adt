
#pragma once
#include "../list/linked_list.hpp"
#include <iostream>
#include <string>

using namespace std;

template <class T>
class Hash{
public:
    size_t operator()(const T value) const{
        return size_t(value);
    }
};

struct KeyNotFoundError : public std::exception{
    const char * message() const throw(){
        return "Key Not Found";
    }
};

template<>
class Hash<std::string>{
public:
    size_t operator()(const std::string key) const{
        unsigned long hash_value = 0;
        for(char el : key){
            hash_value = hash_value*5 + el;
        }
        return size_t(hash_value);
    }
};

template<class K, class E>
struct DPair{
    K key;
    E value;

    DPair(){};
    DPair(K key,E value): key(key), value(value){};

    inline void operator=(E value){
        this->value = value;
    }

    inline void operator=(DPair<K,E>& pair){
        key = pair.key;
        value = pair.value;
    }

    friend std::ostream& operator<<(std::ostream& os, const DPair<K,E>& pair){
        os << "{" << pair.key << " : " << pair.value << "}";
        return os;
    }
};

template<class K, class E>
class Tabella_Hash{
public:

    typedef K key;
    typedef E value;

    // Constructor and destructors 
    Tabella_Hash(size_t);
    Tabella_Hash(const Tabella_Hash<K,E>&);
    ~Tabella_Hash();

    // Operators
    void create(size_t);
    bool empty() const;
    bool insert(const DPair<K,E>&);
    bool insert(K,E);
    void erase(const key&);
    void resize();
    size_t divisor() const { return this->_divisor; }
    size_t size() const{ return this->_lunghezza; }

    size_t value_to_key(const value&) const;
    
    // Overload operands
    template<class KK, class EE>
    friend std::ostream& operator<<(std::ostream&, Tabella_Hash<KK,EE>&);
    DPair<K,E> operator[](const key&) const;
    DPair<K,E>& operator[](const key&); 
    void operator=(const Tabella_Hash<K,E>&);
    bool containsValue(const value&);
    Linked_list<E> values()  ;
    Linked_list<K> keys()  ;
    
private:
    
    Hash<K> fhash;
    size_t _lunghezza;
    size_t _divisor;
    DPair<K,E> **tabella;

    size_t _search(const key&) const;
    void _clear();
    void _copy(const Tabella_Hash<K,E>&);
};



template<class K, class E>
Tabella_Hash<K,E>::Tabella_Hash(size_t divisor){
    create(divisor);
}

template<class K,class E>
Tabella_Hash<K,E>::Tabella_Hash(const Tabella_Hash<K,E>& ht){
    create(ht.divisor());
    this->_lunghezza = ht.size();
    _copy(ht);
}

template<class K, class E>
void Tabella_Hash<K,E>::create(size_t divisor){
    this->_divisor = divisor;
    this->_lunghezza = 0;
    this->tabella = new DPair<K,E>*[divisor];

    for(int i=0; i!=_divisor; i++){
        this->tabella[i] = nullptr;
    }
}

template <class K, class E>
void Tabella_Hash<K,E>::_copy(const Tabella_Hash<K,E>& ht){
    for(int i=0; i!=_divisor; i++){
        if(ht.tabella[i] != nullptr){
            this->tabella[i] = new DPair<K,E>(ht.tabella[i]->key, ht.tabella[i]->value);            
        }else{
            this->tabella[i] = nullptr;
        }
    }
}

template <class K, class E>
void Tabella_Hash<K,E>::_clear(){
    for(int i = 0; i!=_divisor; i++){
    if(tabella[i] != nullptr){
            delete tabella[i];
        }
    }
    delete [] tabella;
}

template <class K, class E>
Tabella_Hash<K,E>::~Tabella_Hash(){
    _clear();
}

template<class K, class E>
std::ostream& operator<<(std::ostream& os, Tabella_Hash<K,E>& ht){
    os << "[";
    bool flag = false;
    for(int i = 0, count = 0; i!= ht._divisor; i++){
        if(ht.tabella[i] != nullptr){
            os << "{" << ht.tabella[i]->key << " : " << ht.tabella[i]->value << "}"; 
            count++;   
            if(count != ht._lunghezza) os << ",";
        }
    }
    os << "]";
    return os;
}


template<class K, class E>
size_t Tabella_Hash<K,E>::_search(const key& key) const{
    size_t i = (size_t) fhash(key) % _divisor;
    size_t j = i;
    do{
        if(tabella[j] == nullptr || tabella[j]->key == key){
            return j;
        }else{
            j = (j+1) % _divisor;
        }
    }while(j!=i);

    return j; 
}

template<class K, class E>
bool Tabella_Hash<K,E>::insert(K key, E element){
    DPair<K,E> pair(key, element);
    return insert(pair);
}

template<class K, class E>
bool Tabella_Hash<K,E>::insert(const DPair<K,E>& pair){
    size_t pos = _search(pair.key);

    if(tabella[pos] == nullptr){
        tabella[pos] = new DPair<K,E>(pair);
        ++_lunghezza;
    }else{
        if(tabella[pos]->key == pair.key){
            tabella[pos]->value = pair.value;
        }else{
            return false;
        }
    }
    return true;
}


template <class K, class E>
DPair<K,E>& Tabella_Hash<K,E>::operator[](const key& key){
    size_t pos = _search(key);

    if(tabella[pos] == nullptr || tabella[pos]->key != key){
        throw KeyNotFoundError();
        
    }else{
        return *tabella[pos];
    }
}

template <class K, class E>
DPair<K,E> Tabella_Hash<K,E>::operator[](const key& key) const{
    size_t pos = _search(key);

    if(tabella[pos] == nullptr || tabella[pos]->key != key){
        throw KeyNotFoundError();
        
    }else{
        return *tabella[pos];
    }
}

template <class K, class E>
void Tabella_Hash<K,E>::erase(const key& key){
    size_t pos = _search(key);

    if(tabella[pos] == nullptr || tabella[pos]->key != key){
        throw KeyNotFoundError();
        
    }else{
        DPair<K,E> *pair = tabella[pos];
        tabella[pos] = nullptr;
        delete(pair);
        --_lunghezza;
    }
}

template <class K,class E>
void Tabella_Hash<K,E>::operator=(const Tabella_Hash<K,E>& ht){
    _clear();
    create(ht.divisor());
    this->_lunghezza = ht.size();
    _copy(ht);
}




template<class K, class E>
bool Tabella_Hash<K,E> ::containsValue(const value& valore) {
    size_t i=0;
    while(i<size()+1){

        if(tabella[i] != nullptr )
            if (tabella[i]->value == valore)
                    return true;
        
    i++;
    }

        return false; 
}




template<class K, class E>
Linked_list<E> Tabella_Hash<K,E>::values()  {
    
    Linked_list<value> l;
        for(int j=0; j<=divisor(); j++) 
            if(tabella[j] != nullptr)
                if(l.find(tabella[j]->value)==nullptr)
                      l.push_back(tabella[j]->value);
            
                
    return l;
}   

template<class K, class E>
Linked_list<K> Tabella_Hash<K,E>::keys() {
   
    Linked_list<key> l;
        for(int j=0; j<=divisor(); j++) 
            if(tabella[j] != nullptr)  
                if(l.find(tabella[j]->key)==nullptr)
                      l.push_back(tabella[j]->key);
            
            
    return l;
}




template<class K, class E>
void Tabella_Hash<K,E>::resize(){
   
    auto div_temp = this->_divisor;   
    this->_divisor= this->_divisor*2;
    DPair<K,E> **tabella1 = new DPair<K,E>*[this->_divisor];
     for(int i=0; i!=div_temp; i++){
        if(this->tabella[i] != nullptr){
           tabella1[i] = new DPair<K,E>(this->tabella[i]->key, this->tabella[i]->value);            
        }else{
            tabella1[i] = nullptr;
        }
    }
    delete [] tabella;
    this->tabella=tabella1;


}


template<class K, class E>
size_t Tabella_Hash<K,E>::value_to_key(const value& value) const{
     for(int j=0; j<=_lunghezza; j++) 
        if(tabella[j] != nullptr)
            if(tabella[j]->value=value)
                return j;

    return -1;
}


