#pragma once

#include <iostream>

template <class T>
class Insieme{
public:
    typedef T tipo_valore;

    //Constructors and destructors
    Insieme();
    Insieme(const Insieme<T>&);
    ~Insieme();

    // Operations
    void crea_insieme();
    int size() const;
    int lenght() const;
    bool insieme_vuoto() const;
    void realloc(int);
    bool cerca(tipo_valore&) const;
    bool inserisci(const tipo_valore&);
    void cancella(tipo_valore&);
    Insieme<T>& unione(const Insieme<T>&) const;
    Insieme<T>& interesezione(const Insieme<T>&) const;
    Insieme<T>& differenza(const Insieme<T>&) const;
    
    // Overload operands
    template <class Z>
    friend std::ostream& operator<<(std::ostream&, const Insieme<Z>&);

private:
    tipo_valore* _elementi;
    int _lunghezza;
    int _size;
};

template <class T>
void Insieme<T>::crea_insieme(){
    _elementi  = new tipo_valore[2];
    _lunghezza = 0;
    _size = 2;
}

template <class T>
Insieme<T>::Insieme(){
    crea_insieme();
}

template <class T>
Insieme<T>::Insieme(const Insieme<T>& cInsieme){
    this->_lunghezza = cInsieme->lenght();
    this->_size = cInsieme->size();
    this->_elementi = new tipo_valore[cInsieme->size()];
    for(int i = 0; i< cInsieme->lenght(); i++){
        this->_elementi[i] = cInsieme->_elementi[i];
    }
}

template <class T>
Insieme<T>::~Insieme(){

}

template <class T>
void Insieme<T>::realloc(int new_size){
    tipo_valore* temp = new tipo_valore[new_size];
    _size = new_size;

    for(int i = 0; i!=_lunghezza; i++){
        temp[i] = _elementi[i];
    }
    delete [] _elementi,
    _elementi = temp;
}

template <class T>
int Insieme<T>::size() const{
    return _size;
}

template <class T>
int Insieme<T>::lenght() const{
    return _lunghezza;
}

template <class T>
bool Insieme<T>::insieme_vuoto() const{
    return (size() == 0);
}

template <class T>
bool Insieme<T>::inserisci(const tipo_valore& value){
    for(int i = 0; i!= _lunghezza; i++){
        if(value == _elementi[i]){
            return 0;
        }
    }
    if(_lunghezza == (_size -1)){
        realloc(_size*2);
    }

    _elementi[_lunghezza] = value;
    _lunghezza++;
    return 1;
}

template <class T>
std::ostream& operator<<(std::ostream& os, const Insieme<T>& st){
    os << "[";
    for(int i=0; i!=st._lunghezza; i++){
        os << st._elementi[i];
        if(i != st._lunghezza - 1){
            os << ",";
        }
    }
    os << "]";
    return os;
}