#include <iostream>
#include "pila.hpp"
#include "pila_vettore.hpp"
#include <string>

using namespace std;    
float evaluate_postfix(string);
string infix_to_postfix(string);
void quicksort(int*,int);
void swap(int &, int &);

int main(){

    // string expr;
    // cin >> expr;
    // cout << evaluate_postfix(expr); 
    // cin >> expr;
    // cout << infix_to_postfix(expr);

    // int a[] = {10,9,8,7,6,5,4,3,2,1};
    // quicksort(a,10);

    // for(int i = 0; i<10; i++){
    //     cout << a[i] << " ";
    // }

    Pila<int> an;
  
    an.push(6);
    an.push(6);
    an.push(8);
    an.push(8);
    an.push(7);

    cout << an.leggi_Pila() << endl;


    cout << an << endl;



  


    return 0;
}

float evaluate_postfix(string expr){
    Pila<float> st;
    for(char &token : expr){
        // Number
        if(token >= '0' && token <= '9'){
            st.push(token - '0');
        }else{
            float operand_1 = st.leggi_Pila(); st.pop();
            float result;

            switch(token){
                case '+':
                    result = st.leggi_Pila() + operand_1;
                    break;
                case '-':
                    result = st.leggi_Pila() - operand_1;
                    break;
                case '/':
                    result = st.leggi_Pila() / operand_1;
                    break;
                case '*':
                    result = st.leggi_Pila() * operand_1;
                    break;
            }
            st.pop();
            st.push(result);
        }
       cout << "[" << token << "]" << st << endl;
    }
    return st.leggi_Pila();
}

string infix_to_postfix(string expr){
    Pila<char> st;
    string output = "";
    for(char token : expr){
        // Operand
        if(token >= '0' && token <= '9'){
            output += token;
        }
        // Operation
        if(token == '+' or token == '-' or token == '/' or token == '*'){
            st.push(token);
        }
        // Close operation
        if(token == ')'){
            output += st.leggi_Pila();
            st.pop();
        }
    }
    return output;    
}

void quicksort(int* a, int n){
    int left, right, i, j, pivot, var;
    Pila<int> pila;
    pila.push(0); pila.push(n-1);

    while(!pila.is_empty()){
        j = right = pila.leggi_Pila(); pila.pop();
        i = left = pila.leggi_Pila(); pila.pop();
        pivot = a[(i+j)/2];

        while(i<=j){
            while(a[i] < pivot) i++;
            while(pivot < a[j]) j--;
            
            if(i<=j){
                if(i != j) swap(a[i],a[j]);
                i++; j--;
            }
        }

        if(left < j){
            pila.push(left);
            pila.push(j);
        }
        if(i < right){
            pila.push(i);
            pila.push(right);
        }
    }
}


void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}