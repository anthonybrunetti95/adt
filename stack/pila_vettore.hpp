
#pragma once
#include <iostream>
#include <cstring>

template <class T>
class Pila_vector {
public:
	typedef T value_type; 
	
	Pila_vector( );
	~Pila_vector();

	void crea_Pila();
	value_type leggi_Pila() const;
	bool pila_vuota() const;
	unsigned int getlenght() const;
	void push(value_type);
	void pop();
	


	template <class Z>
	friend std::ostream& operator<<(std::ostream&, Pila_vector<T>&);
    void print(std::ostream&) const;
    void operator=(const Pila_vector<T>&);

private:
	unsigned int testa,max_Dimensione;
	value_type *vector;
	void cancellaPila();
	void creaPila();
	bool pilaPiena() const;
	unsigned int getTesta() const;

	void cambiaDimensione(int vechiadim,int nuovadim)
	{
		value_type *temp;
		temp=new value_type[nuovadim];
		for(unsigned  int i=0;i<getTesta();i++)
			temp[i]=vector[i];
		delete [] vector;
		vector=temp;
		max_Dimensione=nuovadim;
	}
};

template <class T>
Pila_vector<T>::Pila_vector()
{
	max_Dimensione=10;
	creaPila();
}

template <class T>
void Pila_vector<T>::operator=(const Pila_vector<T>& p){
	Pila_vector<T> pila;
	strcpy(pila,p);
	this->pop();
	max_Dimensione=pila.getlenght();
	
	while(!pila.pila_vuota()){
		this->push(pila.leggi_Pila());
		pila.pop();
	}
}
	
template <class T>
Pila_vector<T>::~Pila_vector()
{
	delete [] vector;
	cancellaPila();
}

template <class T>
void Pila_vector<T>:: cancellaPila() { testa=0; }
	
template <class T>
void Pila_vector<T>:: creaPila()
{
	vector=new  value_type[max_Dimensione];
	testa=0;
}
	
template <class T>
bool Pila_vector<T>::pila_vuota() const { return testa==0;}

template <class T>
bool Pila_vector<T>::pilaPiena() const
{
	return (testa==max_Dimensione);
}
	

template <class T>
unsigned int Pila_vector<T>::getTesta() const { return testa; }

template <class T>
unsigned int Pila_vector<T>::getlenght() const { return getTesta(); }

template <class T>
typename Pila_vector<T>::value_type Pila_vector<T>::leggi_Pila() const{
	if(pila_vuota()) 
		std::cout<<"pila vuota"<<std::endl;
	else
		return vector[testa-1];
}

template <class T>
void Pila_vector<T>::push(value_type elem)
{
	if(pilaPiena()) cambiaDimensione(getlenght(),getlenght()*2);
	vector[testa]=elem;
	testa++;
}

template <class T>
void Pila_vector<T>::pop()
{
	if(pila_vuota()) std::cout<<"pila vuota"<<std::endl;
	else
	{
		if(testa<max_Dimensione/2)
		{
			cambiaDimensione(getlenght(),getlenght()/2);
		}
		testa=testa-1;
	}
}

template <class T>
std::ostream& operator<<(std::ostream& os, Pila_vector<T>& stk){
    os << "[";
    stk.print(os);
    os << "]";
    return os;
}

template <class T>
void Pila_vector<T>::print(std::ostream& os) const{
  int temp = getTesta();
    for(int i=0; i<temp; i++) {
    	os<<vector[i];
    	os<< ",";
    }
}