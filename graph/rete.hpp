#pragma once
#include "graph.hpp"

class rete{
public:
	rete();
	~rete();
	void crearete();
	void aggiungiUtente(GNode <Linked_list<std::string>,int ,std::string> utente);
	bool vuota();
	void conetti(GNode <Linked_list<std::string>,int ,std::string> utente , GNode <Linked_list<std::string>,int ,std::string> utente1);
	void sconetti(GNode <Linked_list<std::string>,int ,std::string> utente, GNode <Linked_list<std::string>,int ,std::string> utente1);
	void posta(GNode <Linked_list<std::string>,int ,std::string> utente,std::string messaggio);
	bool conessi(GNode <Linked_list<std::string>,int ,std::string> utente, GNode <Linked_list<std::string>,int ,std::string> utente1);
	int numAmici( GNode <Linked_list<std::string>,int ,std::string> utente);
	int numMessaggi(GNode <Linked_list<std::string>,int ,std::string> utente);	

private:
	 Graph<Linked_list<std::string>,int,std::string> *grafo;
	 
};

rete::rete() {

}

void rete::crearete() {
	grafo =new Graph<int,Linked_list<std::string>,std::string>(10);
}

void  rete::aggiungiUtente(GNode <Linked_list<std::string>,int ,std::string> utente) {
	grafo->insert_node(new Linked_list<std::string>(),"utente");
}

bool rete::vuota() {
	return grafo->getNodes().empty();
}

void rete::conetti(GNode <Linked_list<std::string>,int ,std::string> utente , GNode <Linked_list<std::string>,int ,std::string> utente1) {
	grafo->insert_edge(utente,utente1,1);

}

void rete::sconetti(GNode <Linked_list<std::string>,int ,std::string> utente , GNode <Linked_list<std::string>,int ,std::string> utente1) {
	grafo->remove_edge(utente,utente1);
}

void rete::posta(GNode <Linked_list<std::string>,int ,std::string> utente,std::string messaggio) {
	auto g = grafo->getNodes().find(utente);
	if(g !=nullptr)
		g.getValue().push_back(messaggio);

}

int numAmici( GNode <Linked_list<std::string>,int ,std::string> utente) {
	return grafo.inDegree(utente) + grafo.outDegree(utente);
}

int numMessaggi(GNode <Linked_list<std::string>,int ,std::string> utente) {
	auto g = grafo.getNodes().find(utente);
	if(g !=nullptr)
		return g.getValue().size();
}