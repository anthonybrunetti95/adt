#pragma once
#include "linked_list.hpp"

template <class T>
class Cluster {
public:
	typedef T tipovalore;
	Cluster();
	void crea();
	void aggiungiCluster(Linked_list<T>&) ;
	void aggiuniOggetto(Linked_list<T>&, tipovalore);
	void rimuoviOggetto(Linked_list<T>&);
	void spostaOggetto(Linked_list<T>&, Linked_list<T>&, tipovalore);
	int nCluster();
private:
	Linked_list<Linked_list<T>>* l;

};


template <class T>
Cluster<T>::Cluster() {
	crea();
}

template <class T>
void Cluster<T>::crea() {
	l = new Linked_list<Linked_list<T>>();
}

template <class T>
void Cluster<T>::aggiungiCluster(Linked_list<T>& cluster) {
	l->push_back(cluster);
}

template <class T>
void Cluster<T>::aggiuniOggetto(Linked_list<T>& cluster, tipovalore elem) {
	auto c = l->find(cluster);
	if(c != nullptr)
		l->read(c).push_back(elem);
}

template <class T>
void Cluster<T>::rimuoviOggetto(Linked_list<T>& cluster) {
	auto c = l->find(cluster);
	if(c != nullptr)
		c.erase(cluster.get_end());
}

template <class T>
void Cluster<T>::spostaOggetto(Linked_list<T>& cl, Linked_list<T>& cl1, tipovalore elem) {
	auto c = l->find(cl);
	if(c != nullptr) {
		auto c1 = l->read(c).find(elem);
		if(c1 != nullptr){
				l->read(c).erase(c1);
				auto d = l->find(cl1);
				if(d!=nullptr) {
					l->read(d).push_back(elem);
				}
			}
		}

}

template <class T>
int Cluster<T>::nCluster() {
	return l->size();
}