#include <iostream>
#include <string>
#include "tree.hpp"

using namespace std;

void inline newline();


struct archo {

public:
    int nodo1 = 0 ;
    int nodo2 = 0; 


} ;

void f_archi(Linked_list<archo> &archi, Tree<int> &tree) {
        tree.insert_root(0);
        auto c = tree.get_root();
        TNode<int>* n;
        TNode<int>* m;
        for(auto arc =archi.begin(); !archi.end(arc); arc=archi.next(arc) ) {
            n =tree.search(archi.read(arc).nodo1);
            if(n!=nullptr) 
                tree.insert_child(n,archi.read(arc).nodo2);
            else {
                m = tree.insert_child(c,archi.read(arc).nodo1);
                tree.insert_child(m,archi.read(arc).nodo2);
            }
            
        }

}



int main() 
{

    Tree<int> mario;
   

  
    mario.insert_root(4);
    auto c =  mario.get_root();
    auto c2 = mario.insert_child(c, 1);
    auto cac = mario.insert_child(c2, 2);
    mario.insert_child(c2, 3);
    auto d = mario.insert_child(c,4);
    auto e = mario.insert_child(d,5);
    cout << mario<<endl;

    Tree<int> luigi;
    luigi.insert_root(100);
    auto s1 = luigi.get_root();
    auto s2 = luigi.insert_child(s1,234);
    auto s3 = luigi.insert_child(s1,245);
    cout << luigi<<endl;
    cout << mario<<endl;
    cout<<mario.n_leaf()<<endl;
    cout<<luigi.n_level(1)<<endl;
    cout<<luigi.width()<<endl;
    Linked_list<archo> archi;
    Tree<int> gigi;
    archo archo1;

    for(int i=1; i<10;i++) {
        archo1.nodo1 = i;
        archo1.nodo2 = i*2;
        archi.push_back(archo1);
    }
    f_archi(archi,gigi);
    cout<<gigi<<endl;

    return 0;
}





