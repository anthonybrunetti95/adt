#include <iostream>
#include "coda.hpp"
#include "../stack/pila.hpp"
#include <string>
#include <sstream>

using namespace std;

void transfer(Pila<char>& stk, Coda<string>& que);
string infix_to_postfix(string expr);
int priority(char operand);

int main(){

    string expr;
    cin >> expr;
    cout << infix_to_postfix(expr);
    Coda<int> c;
    c.push(1);
    c.push(2);
    c.push(3);
    c.push(4);
    cout << c <<endl;
    return 0;
}

string infix_to_postfix(string expr){
    Pila<char> pila;
    Coda<string> coda;
    string output = "";
    string number = "";

    for(char token : expr){
        if(token >= '0' and token <='9'){
            number += token;
        }else if(pila.is_empty() or token == '('){
            pila.push(token);
        }else if(token == ')'){
            while(pila.leggi_Pila() != '('){
                transfer(pila, coda);
            }
            pila.pop();
        }else{
            while(!pila.is_empty() && (priority(token) <= priority(pila.leggi_Pila()))){
                transfer(pila,coda);
            }
            pila.push(token);
        }

        if(token >= '9' or token <= '0'){
            coda.push(number);
            number = "";
        } 
    }

    if(number != "") coda.push(number);

    while(!pila.is_empty()) transfer(pila, coda);
    while(!coda.is_empty()){
        output += coda.leggi_coda() + " ";
        coda.pop();  
    }  

    return output;
}


void transfer(Pila<char>& stk, Coda<string>& que){
    stringstream ss;
    string a;
    ss << stk.leggi_Pila();
    ss >> a;
    que.push(a);
    stk.pop();
}

int priority(char operand){
    switch(operand){
        case '+':
            return 1;
        case '-':
            return 1;
        case '/':
            return 2;
        case '*':
            return 2;
        case '(':
            return 0;
    }

    return -1;
}